Drupal ImageCache per Theme module:
----------------------------------
Maintainers:
  Mark Wise (http://drupal.org/user/501326)
Requires - Drupal 6 and ImageCache
License - GPL (see LICENSE)


Overview:
--------
This module allows theme authors to specify ImageCache presets in their theme's .info file. This module scans all enabled themes in the system for 'theme_imagecache' declarations in the .info file and creates the corresponding ImageCache presets.


Dependencies:
------------

* ImageCache

Installation:
------------
See INSTALL.txt


Configuration:
-------------
There are currently no configuration options available for this module.


Usage:
-----

This module is intended to be used by theme authors. ImageCache presets can be specified by adding 'theme_imagecache' declarations to a theme's .info file. 

The general form of the declaration is as follows:

  theme_imagecache[<preset_name>] = <imagecache_action>:<dimensions>
	

NOTE: This module *only* supports the 'imagecache_scale' and 'imagecache_scale_and_crop' actions. These two actions are (in this author's opinion) satisfactory for most sites.

Examples: (from <themename>.info): 

theme_imagecache[my_large_image] = "imagecache_scale_and_crop:580x320"
theme_imagecache[my_thumbnail] = "imagecache_scale:90"
theme_imagecache[my_thumbnail_tiny] = "imagecache_scale:60x60"
theme_imagecache[node_57] = "imagecache_scale_and_crop:250x150"